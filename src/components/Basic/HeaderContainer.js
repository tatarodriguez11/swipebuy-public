import React from 'react';
import './HeaderContainer.css'

function HeaderContainer(props) {
  return (
    <div className="HeaderContainer">
      {props.children}
    </div>
  )
}

export default HeaderContainer