import React from 'react';
import './UserChat.css';
import Button from '../Basic/UserButton'

class UserChat extends React.Component {
  state = {
    display: "button"
  }

  handleClick = (event) => {
    this.setState({
      display: "text"
    })

    this.props.handleClick(event)

  }

  render() {
    return (

      this.state.display === "button" ?
        <Button
          name="Yes!"
          handleClick={this.handleClick}
        />
        :

        <div className="UserChat">
          <p>{this.props.text}</p>
        </div>
    )
  }
}



export default UserChat;