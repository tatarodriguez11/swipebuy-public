import React from 'react';
import './CompanyChat.css'

const LOADING_TIME = 1500
const INITIAL_TIME = 50
const DISPLAY_TIME = 500;
class CompanyChat extends React.Component {
  state = {
    display: 'initial'
  }


  componentDidMount() {
    setTimeout(() => {
      this.setState({ display: 'loading' })
    }, INITIAL_TIME)
    setTimeout(() => {
      this.setState({ display: 'displaying' })
    }, DISPLAY_TIME)
    setTimeout(() => {
      this.setState({ display: 'loaded' })
    }, LOADING_TIME)
  }

  render() {
    return (
      <div className={`CompanyChatContainer ${this.state.display}`} >
        <div className={`CompanyChat ${this.state.display}`}>
          {this.state.display === "initial" && <p>.</p>}
          {this.state.display === "loading" && <p>...</p>}
          {this.state.display === 'displaying' && <p>{this.props.text}</p>}
          {this.state.display === "loaded" && <p>{this.props.text}</p>}

        </div>
      </ div>
    )
  }
}



export default CompanyChat