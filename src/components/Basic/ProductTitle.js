import React from 'react';
import './ProductTitle.css'

function ProductTitle(props) {
  return (
    <div>
      <h1 className="product-title">{props.title}</h1>
    </div>
  )
}

export default ProductTitle