import React from 'react';
import './UserButton.css'


function UserButton(props) {
  return (
    <div className="user-button-container">
      <button className="user-button" onClick={props.handleClick}>{props.name}</button>
    </div>
  )
}

export default UserButton