import React from 'react';
import './ItemsContainer.css'

function FormInputContainer(props) {
  return (
    <div className="items-container">
      {props.children}
    </div>
  )
}

export default FormInputContainer