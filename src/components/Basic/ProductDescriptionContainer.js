import React from 'react';
import './ProductDescriptionContainer.css'

function ProductDescriptionContainer(props) {
  return (
    <div className="product-description-container">
      {props.children}
    </div>
  )
}

export default ProductDescriptionContainer