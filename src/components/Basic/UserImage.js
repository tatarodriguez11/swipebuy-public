import React from 'react';
import './UserImage.css';

function UserImage() {
  return (



    <img
      src="https://upload.wikimedia.org/wikipedia/en/thumb/d/d8/Michael_J._Fox_as_Marty_McFly_in_Back_to_the_Future%2C_1985.jpg/220px-Michael_J._Fox_as_Marty_McFly_in_Back_to_the_Future%2C_1985.jpg"
      className="UserImage"
      alt="userImage"
    />



  )
}

export default UserImage;