import React from 'react';
import './FormInputContainer.css'

function FormInputContainer(props) {
  return (
    <div className="form-input-container">
      {props.children}
    </div>
  )
}

export default FormInputContainer