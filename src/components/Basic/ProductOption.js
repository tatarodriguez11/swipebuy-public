import React from 'react';
import './ProductOption.css'

function ProductOption(props) {
  return (
    <img
      src={props.src}
      alt="imagen del producto"
      className="ProductOption"
    />


  )
}

export default ProductOption