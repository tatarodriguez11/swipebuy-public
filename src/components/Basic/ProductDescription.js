import React from 'react';
import './ProductDescription.css'

function ProductDescription(props) {
  return (
    <div>
      <p className="product-description">{props.Description}</p>
    </div>
  )
}

export default ProductDescription