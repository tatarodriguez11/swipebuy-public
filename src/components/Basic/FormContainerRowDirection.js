import React from 'react';
import './FormContainerRowDirection.css'

function FormContainerRowDirection(props) {
  return (
    <div className="form-container-row-direction">
      {props.children}
    </div>
  )
}

export default FormContainerRowDirection