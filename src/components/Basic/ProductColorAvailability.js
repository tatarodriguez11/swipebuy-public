import React from 'react';
import './ProductColorAvailability.css'

function ProductColorAvailability(props) {
  return (
    <div className="color-availability-container">
      <div className="color-availability"
        style={{ backgroundColor: props.color }}></div>
      <p className="color-availability-text">{props.textColor}</p>
    </div>
  )
}

export default ProductColorAvailability