import React from 'react';
import './CancelButtonForm.css'

function CancelButtonForm(props) {
  return (
    <div className="cancel-button-container">
      <button className="cancel-button" onClick={props.handleCancelClick}>Cancel</button>
    </div>
  )
}

export default CancelButtonForm