import React from 'react';
import './ProductNormalPrice.css'


function ProductDiscountPrice(props) {
  return (
    <div>
      <h1 className="product-discount-price">${props.discountPrice}</h1>
    </div>
  )
}

export default ProductDiscountPrice