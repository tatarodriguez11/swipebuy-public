import React from 'react';
import './ChatContainer.css';

function ChatContainer(props) {
  return (
    <div className="Container">
      {props.children}
    </div>
  )
}

export default ChatContainer