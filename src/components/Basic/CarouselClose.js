import React from 'react';
import CloseIcon from '../../assets/closewhite.svg'
import './CarouselClose.css'

function CarouselClose(props) {
  return (

    <div>
      <img
        className="carousel-close"
        src={CloseIcon}
        alt=""
        onClick={props.handleCarouselClose}
      />
    </div>
  )
}

export default CarouselClose