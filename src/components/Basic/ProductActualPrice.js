import React from 'react';
import './ProductActualPrice.css'

function ProductPrice(props) {
  return (
    <div>
      <h1 className="product-price">${props.price}</h1>
    </div>
  )
}

export default ProductPrice