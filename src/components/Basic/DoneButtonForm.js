import React from 'react';
import './DoneButtonForm.css'


function DoneButtonForm(props) {
  return (
    <div className="done-button-container">
      <button className="done-button" onClick={props.handleDoneClick} disabled={props.disabled}>Done</button>
    </div>
  )
}

export default DoneButtonForm