import React from 'react';
import './ProductOptionContainer.css'

function ProductOptionContainer(props) {
  return (
    <div className="ProductOption-container" onClick={props.handleProductClick}>

      {props.children}
    </div>

  )
}

export default ProductOptionContainer