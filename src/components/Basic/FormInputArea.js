import React from 'react';
import PropTypes from 'prop-types'
import './FormInputArea.css';


function FormInputArea(props) {
  return (
    <input
      type={props.type}
      className="form-input-area"
      placeholder={props.placeholder}
      onChange={props.onChange}
      value={props.value}
    />
  )
}

FormInputArea.propTypes = {
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

export default FormInputArea