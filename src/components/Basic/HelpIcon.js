import React from 'react';
import QuestionIcon from '../../assets/helpicon.svg'
import './HelpIcon.css'


function HelpIcon(props) {
	return (

		<img
			className="HelpIcon"
			src={QuestionIcon}
			alt=""
		/>

	)
}

export default HelpIcon;