import React from "react";
import { Carousel } from "react-responsive-carousel";

export default () => (
  <Carousel showArrows={false} showStatus={false} showThumbs={false} autoPlay>

    <img
      src="https://assets.xtechcommerce.com/uploads/images/medium/e5d199eb3148a800622b19b09f9e4f1e.jpg"
      alt=""
    />


    <img
      src="http://www.digitaltrends.com/wp-content/uploads/2012/02/back-to-the-future-hoverboard.jpg"
      alt=""
    />

    <img
      src="https://http2.mlstatic.com/cuadro-hoverboard-back-to-the-future-decoracion-marty-mcfly-D_NQ_NP_974320-MLA27269520796_042018-F.jpg"
      alt=""
    />


  </Carousel>
);
