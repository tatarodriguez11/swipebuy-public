import React from 'react';
import './CarouselContainer.css'

function CarouselContainer(props) {
  return (
    <div className="carousel-container">
      {props.children}
    </div>
  )
}

export default CarouselContainer