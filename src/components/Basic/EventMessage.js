import React from 'react';
import './EventMessage.css'

function EventMessage(props) {
  return (
    <div className="event-message-container" onClick={props.handleEventClick}>
      <hr className="event-message-line1" />
      <h2 className="event-message">{props.name}</h2>
      <hr className="event-message-line2" />
    </div>
  )
}

export default EventMessage