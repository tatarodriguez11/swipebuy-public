import React from 'react';
import './ProductOptionTitle.css'

function ProductOptionTitle(props) {
  return (
    <div className="ProductOptionTitle-container">
      <h4 className="ProductOptionTitle">{props.title}</h4>
    </div>
  )
}

export default ProductOptionTitle