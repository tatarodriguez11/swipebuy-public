import React from 'react';
import './FormContainer.css'

function FormContainer(props) {
  return (
    <div className="FormContainer-container">
      <div className={"FormContainer" + (props.out ? " formOut" : '')} >
        {props.children}
      </div>
    </div>
  )
}

export default FormContainer