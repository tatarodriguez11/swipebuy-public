import React from 'react';
import FormContainer from '../Basic/FormContainer';
import FormInput from '../Basic/FormInputArea';
import FormContainerRowDirection from '../Basic/FormContainerRowDirection';
import InputContainer from '../Basic/FormInputContainer';
import DoneButton from '../Basic/DoneButtonForm';
import CancelButton from '../Basic/CancelButtonForm';
import OptionButton from '../Basic/UserButton';
import '../Basic/UserChat.css';
import './AddressForm.css'

class Form extends React.Component {
  state = {
    display: "button",
    showCancel: false,
    enable: false

  }

  handleClick = (event) => {
    this.setState({
      display: "form"
    })

  }
  handleDoneClick = (event) => {
    setTimeout(() => {
      this.setState({
        display: "message",
        showCancel: true
      })

    }, 300)
    this.setState({
      formOut: true
    })
    this.props.handleSendAddressFormClick()

  }

  handleCancelClick = (event) => {
    this.setState({
      display: "message"
    })
    this.props.handleCancelFormClick()


  }

  handleMessageClick = (event) => {
    this.handleClick()
    this.props.handleMessageClick()
  }

  handleChange = (field) => (event) => {
    const { address, apt, city, zip } = this.state
    const isValid = address && apt && city && zip

    this.setState({
      [field]: event.target.value,
      enable: isValid,
    })
  }

  componentDidUpdate() {
    this.props.onStateChange()

  }



  render() {
    return (
      <div className="address-form-container">

        {this.state.display === "button" &&
          <OptionButton
            className="address-form-option-button"
            name="Enter Address"
            handleClick={this.handleClick}
          />}

        {this.state.display === "form" &&

          <FormContainer out={this.state.formOut} >
            <InputContainer>
              <FormInput
                onChange={this.handleChange('address')}
                type="text"
                placeholder="Street address"
                value={this.state.address}
              />
            </InputContainer>
            <InputContainer>
              <FormInput
                onChange={this.handleChange('apt')}
                type="text"
                placeholder="Apt #"
                value={this.state.apt}
              />
            </InputContainer>

            <FormContainerRowDirection>
              <InputContainer>
                <FormInput
                  onChange={this.handleChange('city')}
                  type="text"
                  placeholder="City"
                  value={this.state.city}
                />
              </InputContainer>
              <InputContainer>
                <FormInput
                  onChange={this.handleChange('zip')}
                  type="number"
                  placeholder="ZIP"
                  value={this.state.zip}
                />
              </InputContainer>

            </FormContainerRowDirection>

            <DoneButton
              disabled={!this.state.enable}
              handleDoneClick={this.handleDoneClick}
            />

            {this.state.showCancel === true &&
              <CancelButton handleCancelClick={this.handleCancelClick} />}

          </FormContainer>}

        {this.state.display === "message" &&
          <div
            className="UserChat"
            onClick={this.handleMessageClick}
          >
            <p>{this.state.address},</p>
            <p>{this.state.apt} ,</p>
            <p>{this.state.city} ,</p>
            <p>{this.state.zip}</p>
          </div>
        }

      </div>
    )
  }
}

export default Form
