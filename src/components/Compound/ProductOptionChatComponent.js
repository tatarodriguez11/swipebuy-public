import React from 'react';
import ProductOption from '../Basic/ProductOption';
import ProductOptionContainer from '../Basic/ProductOptionContainer';
import ProductOptionTitle from '../Basic/ProductOptionTitle';
import ProductOptionDetails from '../Basic/ProductOptionDetails';
import { Link } from "react-router-dom";
import './ProductOptionChatComponent.css';

class ProductOptionChatComponent extends React.Component {
  state = {
    title: "",
    src: ""
  }


  render() {
    return (
      <Link to="/ProductDescription" className="Product-option-container-link">
        <ProductOptionContainer >
          <ProductOption
            src={this.props.src}
          />
          <ProductOptionTitle
            title={this.props.title}
          />
          <ProductOptionDetails />
        </ProductOptionContainer>
      </Link>
    )
  }
}

export default ProductOptionChatComponent;