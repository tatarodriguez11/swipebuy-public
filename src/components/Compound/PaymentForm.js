import React from 'react';
import ContainerForm from '../Basic/FormContainer';
import FormInputContainer from '../Basic/FormInputContainer';
import FormInput from '../Basic/FormInputArea';
import DoneButton from '../Basic/DoneButtonForm';
import CancelButton from '../Basic/CancelButtonForm';
import CreditCardInput from 'react-credit-card-input';
import OptionButton from '../Basic/UserButton';
import EventMessage from '../Basic/EventMessage';
import './PaymentForm.css'



class PaymentForm extends React.Component {

  state = {

    display: "button",
    showCancel: false,
    enable: false
  }

  handleChange = (field) => (event) => {
    const { cardNumber, expiry, cvc, email } = this.state
    const isValid = cardNumber && expiry && cvc && email

    this.setState({
      [field]: event.target.value,
      enable: isValid,
    })
  }


  handleClick = (event) => {
    this.setState({
      display: "form"
    })
  }
  handleDoneClick = (event) => {
    this.setState({
      display: "event",
      showCancel: true
    })
    this.props.handleSendPaymentFormClick()
  }

  handleCancelClick = (event) => {
    this.setState({
      display: "event"
    })
    this.props.handleCancelPaymentFormClick()
  }

  handleEventClick = () => {
    this.handleClick()
    this.props.handleMessageClick()
  }

  render() {
    return (
      <div>

        {this.state.display === "button" &&
          <OptionButton
            name="Enter Payment"
            handleClick={this.handleClick} />
        }


        {this.state.display === "form" &&
          <ContainerForm>
            <FormInputContainer>
              <FormInput
                type="email"
                placeholder="Email"
                onChange={this.handleChange('email')}
                value={this.state.email}
              />
            </FormInputContainer>


            <CreditCardInput
              cardNumberInputProps={{ value: this.state.cardNumber, onChange: this.handleChange('cardNumber') }}
              cardExpiryInputProps={{ value: this.state.expiry, onChange: this.handleChange('expiry') }}
              cardCVCInputProps={{ value: this.state.cvc, onChange: this.handleChange('cvc') }}
              fieldClassName="input-credit-card"
              containerClassName="input-credit-card-container"

            />



            <DoneButton
              disabled={!this.state.enable}
              handleDoneClick={this.handleDoneClick}
            />

            {this.state.showCancel === true &&
              <CancelButton handleCancelClick={this.handleCancelClick} />}

          </ContainerForm>}

        {this.state.display === "event" &&
          <EventMessage name="you entered payment" handleEventClick={this.handleEventClick} />
        }
      </div>
    )
  }
}

export default PaymentForm