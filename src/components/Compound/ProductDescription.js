import React from 'react';
import Carousel from "../Basic/Carousel";
import CarouselContainer from '../Basic/CarouselContainer';
import CarouselClose from '../Basic/CarouselClose';
import "react-responsive-carousel/lib/styles/carousel.css";
import ProductDescriptionContainer from '../Basic/ProductDescriptionContainer';
import ProductTitle from '../Basic/ProductTitle';
import ProductActualPrice from '../Basic/ProductActualPrice';
import NormalPrice from '../Basic/ProductNormalPrice';
import RowDirectionContainer from '../Basic/FormContainerRowDirection';
import ItemsContainer from '../Basic/ItemsContainer';
import TextDescription from '../Basic/ProductDescription';
import ProductAvailability from '../Basic/TextAvailability';
import ColorAvailability from '../Basic/ProductColorAvailability';
import {Link } from "react-router-dom";
import './ProductDescription.css'


function ProductDescription (props){  

  return(
     <div className="product-description-container">
        <CarouselContainer>
          <Carousel />
        <Link to="/">
          <CarouselClose/>
          </Link>
        </CarouselContainer>
        <ProductDescriptionContainer>
          <RowDirectionContainer>
            <ProductTitle
              title="HoverBoard v2"
            />
            <ItemsContainer>
              <NormalPrice
                discountPrice={329}
              />
              <ProductActualPrice
                price={249}
              />
            </ItemsContainer>
          </RowDirectionContainer>
          <TextDescription
          Description="Brand new McFly Hoverboard v2 Signature Edition. Autographed by Marty McFly.
Top speed 30 MPH, nuclear powered. "
          />

          <ProductAvailability/>
        <RowDirectionContainer>

          <ColorAvailability
          color="pink"
          textColor="Pink"
          />
          <ColorAvailability
            color="aqua"
            textColor="Neon"
            />
        </RowDirectionContainer>
        </ProductDescriptionContainer>

      </div>
  )
}

export default ProductDescription
