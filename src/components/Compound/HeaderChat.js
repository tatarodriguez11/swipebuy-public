import React from 'react';
import HeaderContainer from '../Basic/Header';
import Username from '../Basic/Username';
import UserImage from '../Basic/UserImage';
import HelpIcon from '../Basic/HelpIcon'

class HeaderChat extends React.Component {
  render() {
    return (
      <HeaderContainer>
        <UserImage />
        <Username />
        <HelpIcon />
      </HeaderContainer>
    )
  }
}

export default HeaderChat;