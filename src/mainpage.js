import React, { Component } from 'react';
import './App.css';
import ChatContainer from './components/Basic/ChatContainer';
import CompanyMessage from '../src/components/Basic/CompanyChat';
import UserMessage from '../src/components/Basic/UserChat';
import PreviewProduct from '../src/components/Compound/ProductOptionChatComponent';
import HeaderChat from '../src/components/Compound/HeaderChat';
import AddressForm from '../src/components/Compound/AddressForm';
import data from '../src/data/data';
import PaymentForm from '../src/components/Compound/PaymentForm';


const CHARGING_TIME = 2000
class MainPage extends Component {
  state = {
    indice: 0,
    messages: []
  }
  timeOutId = null

  componentDidMount() {
    this.setState({
      messages: [...this.state.messages, data[this.state.indice]],
      indice: this.state.indice + 1
    })

    this.scrollToBottom()
  }

  messageTimeOut() {
    if (this.state.indice < data.length) {
      this.timeOutId = setTimeout(() => {
        this.setState({
          messages: [...this.state.messages, data[this.state.indice]],
          indice: this.state.indice + 1
        })
      }, CHARGING_TIME)
    }
  }

  componentDidUpdate() {

    if (this.state.messages[this.state.indice - 1].author === "company") {
      this.messageTimeOut()
    }

    this.scrollToBottom()
  }

  handleUserMessageClick = (event) => {
    this.messageTimeOut()
  }
  handleMessageClick = () => {
    clearTimeout(this.timeOutId)
  }

  scrollToBottom = () => {
    this.messagesEnd.current.scrollIntoView({ behavior: "smooth" });
  }

  messagesEnd = React.createRef()

  render() {
    return (
      <div>

        <HeaderChat />
        <ChatContainer>
          {this.state.messages.map((message) => {
            switch (message.type) {

              case "static":
                return <CompanyMessage text={message.message} />

              case "product":
                return <PreviewProduct title={message.title} src={message.defaultImage} />

              case "option":
                return <UserMessage
                  text={message.message}
                  handleClick={this.handleUserMessageClick}
                />

              case "Address form":
                return <AddressForm
                  handleMessageClick={this.handleMessageClick}
                  handleSendAddressFormClick={this.handleUserMessageClick}
                  handleCancelFormClick={this.handleUserMessageClick}
                  onStateChange={this.scrollToBottom}
                />

              case "Payment Form":
                return <PaymentForm
                  handleMessageClick={this.handleMessageClick}
                  handleSendPaymentFormClick={this.handleUserMessageClick}
                  handleCancelPaymentFormClick={this.handleUserMessageClick}
                />


            }

          }
          )}
          <div ref={this.messagesEnd}
            style={{ float: "right" }}
          >
          </div>


        </ChatContainer>
        {/* <AddressForm /> */}

      </div>

    );
  }
}

export default MainPage;