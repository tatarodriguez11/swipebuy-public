import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import MainPage from './mainpage';
import PagePaymentForm from './PagePaymentForm';
import ProductDescriptionPage from './productdescription';
import './App.css'


function Menu() {
  return (
    <Router>
      <div>


        <Route exact path="/" component={MainPage} />
        <Route path="/ProductDescription" component={ProductDescriptionPage} />
        <Route path="/PagePaymentForm" component={PagePaymentForm} />
      </div>
    </Router>
  )
}

export default Menu;