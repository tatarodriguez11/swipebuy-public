module.exports = [
  {
    type: 'static',
    message: "Hey, It's Marty",
    author: 'company',
    id: 1
  },
  {
    type: "static",
    message: "I'm selling my limited edition hoverboards. Want one?",
    author: "company",
    id: 2
  },
  {
    type: "product",
    author: "company",
    defaultImage: "https://assets.xtechcommerce.com/uploads/images/medium/e5d199eb3148a800622b19b09f9e4f1e.jpg",
    images: [
      "https://assets.xtechcommerce.com/uploads/images/medium/e5d199eb3148a800622b19b09f9e4f1e.jpg",
      "https://image.shutterstock.com/image-vector/pink-hover-board-movie-back-450w-579634552.jpg",
      "https://http2.mlstatic.com/cuadro-hoverboard-back-to-the-future-decoracion-marty-mcfly-D_NQ_NP_974320-MLA27269520796_042018-F.jpg"
    ],
    title: "Hoverboard v2",
    normalPrice: 349,
    discountPrice: 249,
    description: "Brand new McFly Hoverboard v2 Signature Edition. Autographed by Marty McFly.Top speed 30 MPH, nuclear powered.",
    colors: [
      {
        color: "#FF6FA2",
        textColor: "Pink"
      },
      {
        color: "#50E3C2",
        textColor: "Neon"
      }
    ],
    id: 3

  },
  {
    type: "option",
    message: "yes!",
    author: "user",
    id: 4
  },
  {
    type: "static",
    message: "Sweet, where I should ship it to?",
    author: "company",
    id: 5
  },
  {
    type: "Address form",
    message: "Enter Address",
    author: "user",
    id: 6
  },
  {
    type: "static",
    message: "Total is $249 + $20 shipping. What’s your payment info?",
    author: "company",
    id: 7
  },
  {
    type: "Payment Form",
    message: "Enter Payment",
    author: "user",
    id: 8
  },

  {
    type: "static",
    message: "Boom, I emailed you a receipt. You will get the item in 3-5 days",
    author: "company",
    id: 10
  },
  {
    type: "static",
    message: "Seeya!",
    author: "company",
    id: 11
  }
]